# Data Science Portfolio

The main project of this portfolio is to demonstrate my skill solving business challenges though my knowledge and tools of data science.

<div align="center">
    <img alt="portfolio" src="https://images.unsplash.com/photo-1495592822108-9e6261896da8?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1500&q=80" width="100%" height="300">
</div>

<br>

<h1 align="center">Clébio Oliveira Júnior</h1>

<h3 align="center"><i>Physics Teacher and Data Scientist</i></h3>

<div align="center">

[![GitHub Badge](https://img.shields.io/badge/GitHub-100000?style=flat&logo=github&logoColor=white)](https://www.github.com/juniorcl/)&nbsp;&nbsp;
[![Medium Badge](https://img.shields.io/badge/Medium-12100E?style=flat&logo=medium&logoColor=white)](https://www.medium.com/@juniorcl)&nbsp;&nbsp;
[![Kaggle Badge](https://img.shields.io/badge/-Kaggle-23BFFF?style=flat&logo=Kaggle&logoColor=white)](https://www.kaggle.com/juniorcl)&nbsp;&nbsp;
[![Linkedin Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=flat&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/clebiojunior/)&nbsp;&nbsp;
[![Twitter Badge](https://img.shields.io/badge/Twitter-1DA1F2?style=flat&logo=twitter&logoColor=white)](https://www.twitter.com/clebioojunior)&nbsp;&nbsp;
[![DEV Badge](https://img.shields.io/badge/dev.to-0A0A0A?style=flat&logo=dev.to&logoColor=white)](https://www.dev.to/juniorcl/)&nbsp;&nbsp;
[![Gmail Badge](https://img.shields.io/badge/Gmail-D14836?style=flat&logo=gmail&logoColor=white&link=mailto:meigaromlopes@gmail.com)](mailto:clebiomojunior@gmail.com)&nbsp;&nbsp;

</div>

<br>

I'm physics teacher and data scientist. I have master's degress in natural science period during which I developed my research using Python for data analysis and data processing.

Since then, I have studied python, data science, classic machine learning algorithms and neural networks. Therefore, I have made data science projects focused in business solutions with machine learning models.

I have already developed solutions for important business problems such as detecting fraud in transactions, classifying diseases early, identifying customers in churn and prioritizing customers for cross-selling.

The details of each solution are described im data science project section.

### Analytics Tools

* **Data Collection and Storage:** SQL and Postgres.

* **Data Processing and Analytics:** Python, Jupyter and Spyder.

* **Development:** Git, Linux and Docker.

* **Data Visualization:** Seaborn and Matplotlib.

* **Machine Learning Modeling:** Classification, Regression, Time Series, Neural Network, Convolution Neural Network and Natural Language Processing.

* **Machine Learning Deployment:** Flask and Heroku.

## Data Science Projects

* #### [Transaction Fraud Detection](https://github.com/juniorcl/transaction-fraud-detection)

    Bank transaction frauds is one of the most problem for bank operations. So this project uses a data science and machine learning to avoid these kind of transaction. The model got a precision of 0.963 +/- 0.007 and recall 0.763 +/- 0.035. The profit expected by the company is R$ 57,251,574.44.

* #### [Churn Prediction](https://github.com/juniorcl/churn-prediction)

    When a client churns it represents a problem for the company. In this project I created a solution using data to avoid it. The machine learning model got to detect 0.765 of the client which could be churned using unseed data as example. It represents a recovere of R$ 2,878,197.97 for the company.

* #### [Cardiovascular Disease Prediction](https://github.com/juniorcl/cardiovascular-disease-prediction)

    Cardio Catch Dicease is a company especialized in detecting heart disease in early stages. For every 5% to more than 50% in the prediction, there's an increase of R$ 500.00. So, this data science project created a model that recognizes of 0.718 +/- 0.005 and the profit using the model may be about R$ 11,285,500.00.

* #### [Rossman Store Sales](https://github.com/juniorcl/rossman-store-sales)

    To idealize a new strategy of investiments in each sale may be difficult. Therefore, to help the stack holders to make decisions about investiments in each sale. This project of data science created a machine learning model to predict the sales for up to six weeks in advance.

## Kaggle

*  #### [Digit Recognizer with Le-Net 5 Architecture](https://www.kaggle.com/juniorcl/lenet-5-cnn-architecture-digit-recognizer)

    Notebook which I show the creation of the LeNet-5 CNN for the digits recognizer competition. This architecture got a accuracy of 0.99214.   

*  #### [Digit Recognizer Competition with CNN](https://www.kaggle.com/juniorcl/cnn-digit-recognizer-0-99178-score)

    A Notebook which I demonstrate how I created a Convolution Neural Network (CNN) to recognize digits during the Kaggle competition. The neural network got a accuracy of 0.99178.

*  #### [Diabetes Prediction with Tuned Gradient Boosting Model](https://www.kaggle.com/juniorcl/diabetesclassification-tunedgradientboosting-90)

    A tutorial on creating a machine learning model to predict whether a person may be diabetic or not.The Gradient Boosting model got a accuracy of 0.893 +/- 0.024. 

* #### [ENEM Math Score](https://www.kaggle.com/juniorcl/mathenemscores-linearregression-accuracy-90)

    A machine learning project to predict the math score from the ENEM (Exame Nacional do Ensino Médio - National High School Exam). The model got a acuracy of 90%.

